@extends('layout.master')

@section('title')
    Как создавался логотип Мисхакища
@endsection

@section('content')
	<div class="post-body" style="align-items: flex-start;">
		<h1>Как создавался логотип Мисхакища</h1>
		<p>Пробую <a href="//www.fiverr.com">Файвер</a> как средство создания логотипа. Отбираю лучших претендентов и делаю заказ у дизайнерши из Пакистана:</p>
		<blockquote>
			Hi,<br />
			<br />
			Company name: Miskhakische.<br />
			We are a game studio currently targeted towards our first 90s-old-school-style adventure game, but open to other game genres and styles in the future.<br />
			No particular color scheme.<br />
			<br />
			As I see the logo in my mind it should be a text-only logo, but you have all the freedom to design it as you want.<br />
			<br />
			Do your best as you usually do! :)<br />
		</blockquote>
		<p>Получаю первые результаты:</p>
		<img src="/images/ml/01.jpg" /><br />
		<img src="/images/ml/02.jpg" /><br />
		<img src="/images/ml/03.jpg" /><br />
		<p>Закон первого блина налицо. Деликатно прошу подумать еще:</p>
		<blockquote>
			Hi,<br />
			<br />
			To be honest these logo variants are not good enough for my taste right now. Also I expected something really unique (like every average customer does, I guess :) however these variants are not there yet.<br />
			Please try to avoid gradients, text shadows and strokes in the future.<br />
			<br />
			Important points:<br />
			1. Unique<br />
			2. Simple<br />
			<br />
			I like modernism, bauhaus, minimalism. Try to go in that direction (I don't know if it's a good idea, but we can try).<br />
			<br />
			Good work and no offence :)<br />
		</blockquote>
		<p>Второй подход:</p>
		<img src="/images/ml/04.jpg" /><br />
		<img src="/images/ml/05.jpg" /><br />
		<img src="/images/ml/06.jpg" /><br />
		<img src="/images/ml/07.jpg" /><br />
		<img src="/images/ml/08.jpg" /><br />
		<p>Снова не то :( Пробую еще раз объяснить:</p>
		<blockquote>
			Okay, it is still not what I want.<br />
			<br />
			Let's try switching our focus from typography to graphics. I would like you to create some icon/graphic like in your works in attached file.<br />
			<br />
			I'm not quite sure what I want to be there, but I think it should be something that represents one or more of the following (in order of preference):<br />
			<ol>
				<li>adventure games</li>
				<li>old-school video games</li>
				<li>video games</li>
				<li>games in general</li>
				<li>something from 80s and/or 90s</li>
			</ol>
			Hope it helps! :)<br />
			And of course you can let use imagination and intuition and create something that is not related, but still cool. Take for instance LucasArts' "golden guy" logo. Or Sierra's logo. No connection to games or anything but still great logos (especially Sierra's).<br />
		</blockquote>
		<p>Дизайнерша просит отменить заказ, ссылаясь на собственное непонимание моей идеи...</p>
		<p>* * *</p>
		<p>Нахожу других исполнителей. На этот раз это группа дизайнеров. Тоже из Пакистана. Даю обширное техзадание, основанное на предыдущем опыте.</p>
		<blockquote>
			Hi,<br />
			<br />
			Company name: Miskhakische.<br />
			We are a game studio currently targeted towards our first 90s-old-school-style adventure game, but open to other game genres and styles in the future.<br />
			No particular color scheme.<br />
			We need a simple unique icon based logo (no typography is required at his point, but you are welcome to add it as well).<br />
			<br />
			It should represent one or more of the following:
			<ol>
				<li>adventure games</li>
				<li>old-school video games</li>
				<li>video games</li>
				<li>games in general</li>
				<li>something from 80s and/or 90s</li>
			</ol>
			<br />
			And of course you can freely use imagination and intuition to create something that is not related, but still cool.
		</blockquote>
		<p>Дизайнеры просят "вдохновляющие" лого. Я пишу, что не знаю о чем они. Тем временем рождается первый шедевр:</p>
		<img src="/images/ml/09.jpg" /><br />
		<p>Наш ответ Чемберлену:</p>
		<blockquote>
			1. Ok, this design is still not what I want.<br />
			2. Actually I expected from you to think of some cool metaphor :)<br />
			3. I can can think of something more specific and get back to to you.<br />
			<br />
			Thank you.<br />
		</blockquote>
		<p>* * *</p>
		<p>Начинаю работать над собственными идеями. На ум приходят три варианта:</p>
		<img src="/images/ml/11.png" /><br />
		<img src="/images/ml/12.png" /><br />
		<img src="/images/ml/13.png" /><br />
		<p>Советуюсь с Маргом. Оба выбираем клавиши.</p>
		<p>* * *</p>
		<p>Тем временем дизайнеры выдают новый вариант:</p>
		<img src="/images/ml/14.jpg" /><br />
		<p>* * *</p>
		<p>Пока шел вариант дизайнеров, мой вариант оброс новыми метафорами. Шлю дизайнерам свой вариант. Пусть разовьют идею, доведут до ума, вдохнут жизнь...</p>
		<img src="/images/ml/15.png" /><br />
		<p>* * *</p>
		<p>А тем временем в Пакистане...</p>
		<img src="/images/ml/16.jpg" /><br />
		<p>Прошу послать с нормальным углом зрения, а то хер поймешь что.</p>
		<img src="/images/ml/17.jpg" /><br />
		<p>Все равно хер поймешь что. Прошу посмотреть как можно улучшить лого, которое послал я. Результат:</p>
		<img src="/images/ml/18.jpg" /><br />
		<p>На этом мы с пакистанскими дизайнерами распрощались.</p>
		<p>* * *</p>
		<p>Добавляю метафор и веселых цветов и в таком виде шлю на Бизнес-линч:</p>
		<img src="/images/ml/19.png" /><br />
		<p>Пока Лебедев и его команда ломают голову над рецензией, решаю развивать дальше то, что есть:</p>
		<img src="/images/ml/20.png" /><br />
		<p>Слишком по-военному как-то.</p>
		<p>* * *</p>
		<p>Решаю начать с чистого листа.</p>
		<p>Пробую другого дизайнера. Когда-то он сделал мне удачный логотип (правда, он был во многом уже проработан мной).</p>
		<p>Вариант 1:</p>
		<img src="/images/ml/21.jpg" /><br />
		<p>Не то:</p>
		<blockquote>
			<p>I don't really like the colours. (Also, it resembles Motorola logo)<br />
			I don't see any interesting meaning (the letter M's different widths are quite meaningless). As you maybe noticed from my own ideas, I'm looking for some gimmick in the logo.</p>
			<p>The logo doesn't have to contain the letter M, by the way (look at Sierra's logo, for example).</p>
			<p>Maybe this one is a good variant, but lets try something else.</p>
		</blockquote>
		<p>Вариант 2:</p>
		<img src="/images/ml/22.jpg" /><br />
		<p>Ичо?.. (Уже пытаюсь поделикатнее выбраться из этого заказа.)</p>
		<blockquote>
			<p>Guess, I like the lack of specifics in this logo, but this logo is already widely recognised as a Command symbol (and few other ones), so it can't be unique.</p>
		</blockquote>
		<p>Вариант 3:</p>
		<img src="/images/ml/23.jpg" /><br />
		<p>Похоже, действительно удача того другого лого была просто удачей. Прощаемся.</p>
		<p>* * *</p>
		<p>Тут еще выясняется, что лого с разноцветными кнопками уже есть</p>
		<img src="/images/ml/24.png" /><br />
		<p>* * *</p>
		<p>Пытаюсь отойти от направления знака и больше концентрируюсь на текстовом варианте. (Даже придумываю сокращенную версию из трех букв.)</p>
		<img src="/images/ml/25.jpg" /><br />
		<p>Игры с буквами выглядят бессмысленными. Пробуем более строгий вариант:</p>
		<img src="/images/ml/26.jpg" /><br />
		<p>Получилась надпись над въездом в концлагерь.</p>
		<p>Пробую разные варианты написания, пытаюсь облегчить шрифт.</p>
		<img src="/images/ml/27.jpg" /><br />
		<p>Попутно родилась идея с кружочком. Круг отвлекает от остатка слишком многобуквенного названия. Кроме того круг вместе с содержимым может служить [фав]иконкой.</p>
		<img src="/images/ml/28.jpg" /><br />
		<p>Все не то. Не про нас это.</p>
		<p>* * *</p>
		<p>Другая идея.</p>
		<img src="/images/ml/29.png" /><br />
		<img src="/images/ml/30.png" /><br />
		<img src="/images/ml/31.png" /><br />
		<p>Две проблемы (минимум):</p>
		<ol>
			<li>
				Не про нас (и вообще непонятно про кого). Ну, может тут есть намек на оригами, а это как-то худо-бедно связано с играми...
			</li>
			<li>
				Слишком в стиле материал-дизайна, который через пару лет (месяцев?) выйдет из моды. Это все равно что в 2015 году делать дизайн в стиле скевоморфизма.
			</li>
		</ol>
		<p>* * *</p>
		<p>Внезапно появляется идея простого знака. Это буква М, и она напоминает головоломку, фигуру из тетриса, пиксели и даже (отдаленно) пульт от консоли. Хорош в любом цвете и при любом размере.</p>
		<img src="/images/ml/32.png" /><br />
		<p>Добавляем по возможности позитивные цвета, строчные буквы (нейтрализуем тяжеловесность названия).</p>
		<img src="/images/ml/33.jpg" /><br />
		<p>Шрифт Noto Sans позволяет написать текст практически на любом языке. Делаем несколько локализованных вариантов.</p>
		<p>Русский:</p>
		<img src="/images/ml/34.jpg" /><br />
		<p>Украинский:</p>
		<img src="/images/ml/35.jpg" /><br />
		<p>Иврит:</p>
		<img src="/images/ml/36.jpg" /><br />
		<p>Греческий:</p>
		<img src="/images/ml/37.jpg" /><br />
		<p>Японский:</p>
		<img src="/images/ml/38.jpg" /><br />
		<p>Грузинский:</p>
		<img src="/images/ml/39.jpg" /><br />
		<p>Армянский:</p>
		<img src="/images/ml/40.jpg" /><br />
		<p>Амхарский:</p>
		<img src="/images/ml/41.jpg" />
	</div>
@endsection