@extends('layout.master')

@section('title')
	Логин
@endsection

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/login.css">
@endsection

@section('content')
	<div id="content-wrapper">
		<div id="sign-in">
			<h3>Вход</h3>
			<form action="{{ route('signin') }}" method="post">
				<div style="display:flex; flex-direction: column;">
					<label for="username">Юзернейм</label>
					<input type="username" name="username" id="username" value="{{ Request::old('username') }}" class="{{ $errors->has('username') ? 'error-field' : '' }}" />
				</div>

				<div style="display:flex; flex-direction: column;">
					<label for="password">Пассворд</label>
					<input type="password" name="password" id="password" value="{{ Request::old('password') }}" class="{{ $errors->has('password') ? 'error-field' : '' }}" />
				</div>
				<button type="submit">Поехали</button>
				<input type="hidden" name="_token" value="{{ Session::token() }}" />
			</form>
		</div>
	</div>
	@include('includes.message-block')
	
@endsection