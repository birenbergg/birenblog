@extends('layout.master')

@section('title', 'Редактирование')

@section('main_title')
	<a href="/outdoors">Поездки</a>
@stop

@section('content')
	<div>
		{{ HTML::ul($errors->all()) }}

		{{ Form::model($post, array('route' => array('outdoors.update', $post->id), 'method' => 'PUT')) }}

			<div class="edit-block double">
				<div>
					{{ Form::label('title', 'Заголовок') }}
					{{ Form::text('title', null) }}
				</div>
				<div>
					{{ Form::label('list_title', 'Заголовок для списка') }}
					{{ Form::text('list_title', null) }}
				</div>
			</div>
			<div class="edit-block">
				{{ Form::label('slug', 'Кодовое имя') }}
				{{ Form::text('slug', null) }}
			</div>
			<div class="edit-block">
				{{ Form::label('body', 'Текст') }}<br />
				{{ Form::textarea('body', null) }}
			</div>
			<div class="edit-block double">
				<div>
					{{ Form::label('part_number', 'Номер части') }}
					{{ Form::text('part_number', null) }}
				</div>
				<div>
					{{ Form::label('parts_number', 'Всего частей') }}
					{{ Form::text('parts_number', null) }}
				</div>
			</div>
			<div class="edit-block">
				<div>
					{{ Form::label('sequel_ready', 'Следующий пост готов') }}
					{{ Form::select('sequel_ready', array('0' => 'Нет', '1' => 'Да'), null) }}
				</div>
			</div>
			<div class="edit-block">
				<div>
					{{ Form::label('slug_without_part_number', 'Кодовое имя поста без номера части') }}
					{{ Form::text('slug_without_part_number', null) }}
				</div>
			</div>
			<div class="edit-block">
				<div>
					{{ Form::label('custom_sequel_slug', 'Кодовое имя следующего поста (если отличается от шаблона)') }}
					{{ Form::text('custom_sequel_slug', null) }}
				</div>
			</div>
			<div class="edit-block double">
				<div>
					{{ Form::label('travel_date_start', 'Дата путешествия (начало)') }}
					{{ Form::date('travel_date_start', null) }}
				</div>
				<div>
					{{ Form::label('travel_date_end', 'Дата путешествия (конец)') }}
					{{ Form::date('travel_date_end', null) }}
				</div>
			</div>
			<div class="edit-block double">
				<div>
					{{ Form::label('year_start', 'Год путешествия (начало)') }}
					{{ Form::text('year_start', null) }}
				</div>
				<div>
					{{ Form::label('year_end', 'Год путешествия (конец)') }}
					{{ Form::text('year_end', null) }}
				</div>
			</div>
			<div class="edit-block double">
				<div>
					{{ Form::label('month_start', 'Месяц путешествия (начало)') }}
					{{ Form::text('month_start', null) }}
				</div>
				<div>
					{{ Form::label('month_end', 'Месяц путешествия (конец)') }}
					{{ Form::text('month_end', null) }}
				</div>
			</div>
			<div class="edit-block">
				{{ Form::label('status', 'Статус') }}<br />
				{{ Form::select('status', array('3' => 'Черновик', '1' => 'Поста не будет (только запись в списке)', '2' => 'Готовый пост'), null) }}
			</div>
			<div class="edit-block">
				<div>
					{{ Form::label('date', 'Дата и время публикации') }}
					{{ Form::text('date', null) }}
				</div>
			</div><div class="edit-block">
			<div>Строение поста</div>
			@foreach($post->components as $c)
			<div style="display:flex; background:#ccc; padding:.5em; margin:.5em 0; border-radius: .4em">
				<div style="width:50%; margin-right: 1em;">
					<textarea>{{ $c->p }}</textarea>
				</div>
				<div style="width:50%; display: flex; align-items: center; flex-wrap: wrap;">
					@if($c->img !== null)
						<img style="margin-right: .5em;" src="/images/outdoors/{{ $post->slug }}/{{ str_pad($c->img, 2, '0', STR_PAD_LEFT) }}.jpg" />
					@elseif(count($c->slideshow_images) > 0)
						@foreach($c->slideshow_images as $img)
							<img style="margin-right: .5em;" src="/images/outdoors/{{ $post->slug }}/{{ str_pad($img->img, 2, '0', STR_PAD_LEFT) }}.jpg">
						@endforeach
					@endif
						<span style="width:100px; height: 100px; background: #ddd;display:inline-flex;font-size: 75px; color: #bbb;font-weight:bold;cursor:pointer;justify-content: center;
align-items: center;

">+</span>
				</div>
			</div>
			@endforeach
		</div>

			{{ Form::submit('Сохранить', array('class' => 'save')) }}
		{{ Form::close() }}
		
		{{ Form::open(array('url' => 'outdoors/' . $post->id)) }}
			{{ Form::hidden('_method', 'DELETE') }}
			{{ Form::submit('Удалить', array('class' => 'delete')) }}
		{{ Form::close() }}
	</div>
@stop