@extends('layout.master')
@section('title', 'Поездки')
@section('main_title', 'Поездки')

@section('content')
	<h1>Поездки</h1>
	@if(Auth::user() && Auth::user()->id == 1)
		<div class="new-post-button-wrapper">
			<a href="{{ route('outdoors.create') }}">Новый пост</a>
		</div>
	@endif
	<div style="margin-bottom:4em;">
		<p class="hyphenated">Я родился в Одессе. В 1990 году репатриировался в Израиль, куда добрался через Москву и Будапешт. С 1992 по 1994 таки снова жил в Одессе.</p>
		<p class="hyphenated">С 2009 года я составляю отчеты о поездках и походах и публикую их на сайте.</p>
		<p class="hyphenated">Помимо этого, я решил составить список мест, которые я посещал и в которых бывал с осени 1990 года. Список не является и не может быть полным, поскольку бывал я в большом количестве мест (достаточно большом, чтобы не запомнить их все), и естественно, не заботился о регистрации каждой поездки в специальном блокноте.</p>
	</div>
	<div>
		@foreach($data as $years)
			<h3 class="year">{{ $years[0]->year_start }}</h3>
				<div class="year-group">
					@foreach($years as $post)
		 				@if ($post->status != 0)
							<div class="outdoors-li">
								@if($post->status != 2 && !(Auth::check() && $post->status == 3))
									<span>{!! $post->list_title == '' ? $post->title : $post->list_title !!}</span>
								@endif
								@if((Auth::check() && $post->status == 3) || $post->status == 2)
									<span><a class="{{ Auth::check() && $post->status == 3 ? 'hidden-link' : '' }}" href="/outdoors/{{ $post->slug }}">{!! $post->list_title == '' ? $post->title : $post->list_title !!}</a></span>
								@endif
								<span class="full-month" style="color:#aaa;font-size:smaller;padding: 0 8px">
									{{ date_range_without_year($post->travel_date_start, $post->travel_date_end, 'ru') }}
								</span>
								<br />
								<br />
							</div>
						@endif
					@endforeach
				</div>
		@endforeach
	</div>
@stop