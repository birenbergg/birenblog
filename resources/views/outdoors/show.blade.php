@extends('layout.master')

@section('title')
	{{ $post->title }}
@stop

@section('main_title')
	<a href="/outdoors">Поездки</a>
@stop

@section('styles')
	@parent
	<link rel="stylesheet" type="text/css" href="/css/slideshow.css">
	<link rel="stylesheet" type="text/css" href="/css/new-slideshow.css">
@stop

@section('content')
	<div>
		<h2 class="post-title">{{ $post->title }}</h2>
		<date class="post-date">{{ date_range($post->travel_date_start, $post->travel_date_end) }}</date>
		
		@if(Auth::user() && Auth::user()->id == 1)
			<div class="admin-buttons">
				<div>
					<a href="/outdoors/{{ $post->slug }}/edit">Редактировать</a>
				</div>
			</div>
		@endif

		<div class="post-body outdoors-post list">
			@if($post->has_components)
				<div>
					@foreach($post->components as $c)
						<p>{{ $c->p }}</p>
						@if(count($c->slideshow_images) == 1)
							<img src="/images/outdoors/{{ $post->slug }}/{{ str_pad($c->slideshow_images[0]->img, 2, '0', STR_PAD_LEFT) }}.jpg" />
						@elseif(count($c->slideshow_images) > 0)
							<div class="slideshow-container">
								@foreach($c->slideshow_images as $img)
									<div class="mySlides">
										<img src="/images/outdoors/{{ $post->slug }}/{{ str_pad($img->img, 2, '0', STR_PAD_LEFT) }}.jpg" style="width:100%">
									</div>
								@endforeach

								<a class="prev" onclick="plusSlides(-1)">❮</a>
								<a class="next" onclick="plusSlides(1)">❯</a>

								<div style="text-align:center; position: relative; bottom: 64px">
									@foreach($c->slideshow_images as $img)
										<span class="dot" onclick="currentSlide({{ $loop->index + 1 }})"></span>
									@endforeach
								</div>
							</div>
						@endif
					@endforeach
				</div>
			@else
				<div>{!! $post->body !!}</div>
			@endif
		</div>

		@if($post->parts_number && $post->part_number < $post->parts_number)
			<p>
				@if($post->sequel_ready)<a href="/outdoors/{{ $post->custom_sequel_slug ? $post->custom_sequel_slug : ($post->slug_without_part_number . '-' . ($post->part_number + 1)) }}" />@endif

				@if($post->part_number == $post->parts_number - 1)Окончание@elseПродолжение@endif<!--
			 -->@if($post->sequel_ready)</a>@endif
				следует.
			</p>
		@endif

		@if(count($post->comments) > 0)
			<div id="comments">
				<h3>{{ count($post->comments) }} камент(а|ов)</h3>
				<div>
					@foreach ($post->comments as $comment)
						<div>
							<h4>
								{{ $comment->poster_name }}
								<date>{{ full_ru_datetime($comment->date) }}</date>
							</h4>
							<blockquote>
								{!! $comment->body !!}
							</blockquote>
						</div>
					@endforeach
				</div>
			</div>
		@endif
	</div>
@stop