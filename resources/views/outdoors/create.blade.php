@extends('layout.master')

@section('title', 'Новый пост')

@section('main_title')
	<a href="/outdoors">Поездки</a>
@stop

@section('content')
	<div>
		{{ HTML::ul($errors->all()) }}

		{{ Form::open(array('url' => 'outdoors')) }}

			<div class="edit-block double">
				<div>
					{{ Form::label('title', 'Заголовок') }}
					{{ Form::text('title', null) }}
				</div>
				<div>
					{{ Form::label('list_title', 'Заголовок для списка') }}
					{{ Form::text('list_title', null) }}
				</div>
			</div>
			<div class="edit-block">
				{{ Form::label('slug', 'Кодовое имя') }}
				{{ Form::text('slug', null) }}
			</div>
			<div class="edit-block">
				{{ Form::label('body', 'Текст') }}<br />
				{{ Form::textarea('body', null) }}
			</div>
			<div class="edit-block double">
				<div>
					{{ Form::label('part_number', 'Номер части') }}
					{{ Form::text('part_number', null) }}
				</div>
				<div>
					{{ Form::label('parts_number', 'Всего частей') }}
					{{ Form::text('parts_number', null) }}
				</div>
			</div>
			<div class="edit-block">
				<div>
					{{ Form::label('sequel_ready', 'Следующий пост готов') }}
					{{ Form::select('sequel_ready', array('0' => 'Нет', '1' => 'Да'), null) }}
				</div>
			</div>
			<div class="edit-block">
				<div>
					{{ Form::label('slug_without_part_number', 'Кодовое имя поста без номера части') }}
					{{ Form::text('slug_without_part_number', null) }}
				</div>
			</div>
			<div class="edit-block">
				<div>
					{{ Form::label('custom_sequel_slug', 'Кодовое имя следующего поста (если отличается от шаблона)') }}
					{{ Form::text('custom_sequel_slug', null) }}
				</div>
			</div>
			<div class="edit-block double">
				<div>
					{{ Form::label('travel_date_start', 'Дата путешествия (начало)') }}
					{{ Form::date('travel_date_start', null) }}
				</div>
				<div>
					{{ Form::label('travel_date_end', 'Дата путешествия (конец)') }}
					{{ Form::date('travel_date_end', null) }}
				</div>
			</div>
			<div class="edit-block double">
				<div>
					{{ Form::label('year_start', 'Год путешествия (начало)') }}
					{{ Form::text('year_start', null) }}
				</div>
				<div>
					{{ Form::label('year_end', 'Год путешествия (конец)') }}
					{{ Form::text('year_end', null) }}
				</div>
			</div>
			<div class="edit-block double">
				<div>
					{{ Form::label('month_start', 'Месяц путешествия (начало)') }}
					{{ Form::text('month_start', null) }}
				</div>
				<div>
					{{ Form::label('month_end', 'Месяц путешествия (конец)') }}
					{{ Form::text('month_end', null) }}
				</div>
			</div>
			<div class="edit-block">
				{{ Form::label('status', 'Статус') }}<br />
				{{ Form::select('status', array('3' => 'Черновик', '1' => 'Поста не будет (только запись в списке)', '2' => 'Готовый пост'), null) }}
			</div>
			<div class="edit-block double">
				<div>
					{{ Form::label('custom_date', 'Задать дату публикации вручную') }}
					{{ Form::checkbox('custom_date', null) }}
				</div>
				<div>
					{{ Form::label('date', 'Дата и время публикации') }}
					{{ Form::text('date', null) }}
				</div>
			</div>

			{{ Form::submit('Создать', array('class' => 'save')) }}
		{{ Form::close() }}
	</div>
@stop