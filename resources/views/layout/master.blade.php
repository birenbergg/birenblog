<!DOCTYPE html>
<html lang="ru">
	<head>
		<title>@yield('title')</title>
		<meta charset="utf-8" />

		<link rel="apple-touch-icon-precomposed" href="/images/icons/iphone.png" />
		<link rel="icon" type="image/png" href="/images/icons/favicon.png" />

		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

		<link rel="stylesheet" type="text/css" href="/css/site.css">
		<link rel="stylesheet" type="text/css" href="/css/general-layout.css">
		<link rel="stylesheet" type="text/css" href="/css/header.css">
		<link rel="stylesheet" type="text/css" href="/css/footer.css">

		<link rel='stylesheet' media='screen and (min-height: 0px) and (max-height: 950px)' href='/css/image-height-750.css' />
		<link rel='stylesheet' media='screen and (min-height: 0px) and (max-height: 750px)' href='/css/image-height-550.css' />

		<link rel='stylesheet' media='screen and (max-width: 1365px)' href='/css/max-width-1365.css' />
		<link rel='stylesheet' media='screen and (max-width: 1024px)' href='/css/max-width-1023.css' />
		<link rel='stylesheet' media='screen and (max-width: 767px)' href='/css/max-width-767.css' />

		<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' />

		@yield('styles')

		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body dir="auto">
		<header ng-app ng-init="mobileMenuHidden = true">
			@include('layout.header')
		</header>
		<main id="@yield('mainId')" class="@yield('mainClass')">
			@yield('content')
		</main>
		<footer>
			<div id="copyright">Copyright &copy; 2013&ndash;<?= date('Y'); ?>, GZM Arts</div>
		</footer>
	
		<!-- AngularJS + components -->
		<script src="/js/angular.min.js"></script>
		<script src="/js/app.js"></script>
		<script src="/js/blogController.js"></script>

		<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
		<script src="/js/slideshow.js"></script>
		<script src="/js/new-slideshow.js"></script>
		<script src="/js/kokhav-yarden.js"></script>
	</body>
</html>