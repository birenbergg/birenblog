<!-- <h1>
	<div>
		<a href="/"><span class="title-home-link">Сайт</span></a><span class="title-home-link"> /</span> @yield('title_on_page')
		@if (Auth::check())
			<span id="logout"><a href="{{ url('logout') }}">Выйти</a></span>
		@endif
	</div>
</h1> -->


<div id="menu">
	<div style="display: flex; flex-grow: 1; align-items: center;justify-content: center;">

		@if(!Route::is('blog.index') || $posts->currentPage() > 1)
		<a href="{{ route('blog.index') }}">
		@endif
			<img src="http://www.birenbergg.com/images/genchik.jpg" id="logo" />
		@if(!Route::is('blog.index') || $posts->currentPage() > 1)
		</a>
		@endif
		<h1>@yield('main_title')</h1>

		<ul id="menu-ul">
			<li>
				@if(!Route::is('blog.index') || $posts->currentPage() > 1)
				<a href="{{ route('blog.index') }}">
				@endif
					Бл-г
				@if(!Route::is('blog.index') || $posts->currentPage() > 1)
				</a>
				@endif
			</li>

			<li>
				@if(!Route::is('outdoors.index'))
				<a href="{{ route('outdoors.index') }}">
				@endif
					Поездки
				@if(!Route::is('outdoors.index'))
				</a>
				@endif
			</li>

			<li>
				@if(!Route::is('rest.index'))
				<a href="{{ route('rest.index') }}">
				@endif
					Ресторанная критика
				@if(!Route::is('rest.index'))
				</a>
				@endif
			</li>

			<li>
				@if(!Route::is('hrenotive.index'))
				<a href="{{ route('hrenotive.index') }}">
				@endif
					Хренотив
				@if(!Route::is('hrenotive.index'))
				</a>
				@endif
			</li>

			<li>
				@if(!Route::is('notebook.index'))
				<a href="{{ route('notebook.index') }}">
				@endif
					Записная книжка
				@if(!Route::is('notebook.index'))
				</a>
				@endif
			</li>

			<li>
				@if(!Route::is('zhitie.index') || $posts->currentPage() > 1)
				<a href="{{ route('zhitie.index') }}">
				@endif
					Житие Шухера
				@if(!Route::is('zhitie.index') || $posts->currentPage() > 1)
				</a>
				@endif
			</li>
		</ul>
	</div>
	<img id="show-menu" src="/images/icons/menu.svg" onclick="document.getElementsByTagName('header')[0].className += ' mobile';" />
	<img id="hide-menu" src="/images/icons/cancel.svg" onclick="document.getElementsByTagName('header')[0].className = document.getElementsByTagName('header')[0].className.replace(/\bmobile\b/,'');" />
</div>