@extends('layout.master')

@section('title', 'Новая запись')

@section('main_title')
	<a href="/notebook">Записная книжка</a>
@stop

@section('content')
	<div>
		{{ HTML::ul($errors->all()) }}

		{{ Form::open(array('url' => 'notebook')) }}
			<div class="edit-block">
				<div>
					{{ Form::label('body', 'Текст') }}
					{{ Form::text('body', Input::old('body')) }}
				</div>
			</div>

			{{ Form::submit('Создать', array('class' => 'save')) }}
		{{ Form::close() }}
	</div>
@stop