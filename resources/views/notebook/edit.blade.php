@extends('layout.master')

@section('title', 'Редактирование')

@section('main_title')
	<a href="/notebook">Записная книжка</a>
@stop

@section('content')
	<div>
		{{ HTML::ul($errors->all()) }}

		{{ Form::model($note, array('route' => array('notebook.update', $note->id), 'method' => 'PUT')) }}
			<div class="edit-block">
				<div>
					{{ Form::label('body', 'Текст') }}
					{{ Form::text('body', Input::old('body')) }}
				</div>
			</div>

			{{ Form::submit('Сохранить', array('class' => 'save')) }}
		{{ Form::close() }}
		
		{{ Form::open(array('url' => 'notebook/' . $note->id)) }}
			{{ Form::hidden('_method', 'DELETE') }}
			{{ Form::submit('Удалить', array('class' => 'delete')) }}
		{{ Form::close() }}
	</div>
@stop