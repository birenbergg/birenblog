@extends('layout.master')
@section('title', 'Записная книжка')
@section('main_title', 'Записная книжка')

@section('content')
	<h1>Записная книжка</h1>

	@if(Auth::user() && Auth::user()->id == 1)
		<div class="new-post-button-wrapper">
			<a href="{{ route('notebook.create') }}">Новая запись</a>
		</div>
	@endif

	@foreach($notes as $note)
		<p>
			{!! $note->body !!}
			@if(Auth::user() && Auth::user()->id == 1)
				<a href="/notebook/{{ $note->id }}/edit">Редактировать</a>
			@endif	
		</p>
	@endforeach
@stop