@extends('layout.master')

@section('title')
	{{ $hrenotive->title_ru }}
@stop

@section('main_title')
	<a href="/hrenotive">Хренотив</a>
@stop

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/hrenotive.css">
@stop

@section('mainClass', 'nostretch')

@section('content')
	<h2 style="margin-top:0;" class="post-title">{{ $hrenotive->title_ru }}</h2>

	<date class="post-date">{{ full_ru_date($hrenotive->date) }}</date>

	<img class="hrenotive" src="/images/hrenotive/{{ $hrenotive->slug }}.jpg">
@stop