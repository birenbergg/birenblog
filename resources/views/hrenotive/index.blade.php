@extends('layout.master')

@section('title', 'Хренотив')
@section('main_title', 'Хренотив')
@section('mainClass', 'blog')

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/hrenotive.css">
@stop

@section('content')
	<h1 style="margin-bottom: 10px;">Хренотив</h1>
	<p style="font-size: 18px; text-align: center; margin-bottom: 60px;">Сборник попыток визуально шутить</p>
	<img src="/images/hrenotive/hrenoten.jpg" style="align-self: center; margin-bottom: 4em; border: 1px solid #ccc;">
    <div class="hreno cols">
		@foreach($hrenotives as $hrenotive)
			<div class="hreno-thumbnail{{ $hrenotive->status_ru == 1 ? ' hidden-hrenotive' : '' }}">
				<a href="/hrenotive/{{ $hrenotive->slug }}">
					<img src="/images/hrenotive/thumbnails/{{ $hrenotive->slug }}.jpg" alt="{{ $hrenotive->title_ru }}" /></a>
				<br />
				<p>
					<a href="/hrenotive/{{ $hrenotive->slug }}">{{ $hrenotive->title_ru }}</a>
				</p>
			</div>
		@endforeach
    </div>
@stop