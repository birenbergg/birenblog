@if(count($errors) > 0)
	<div class="error-message">
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif
@if(Session::has('message'))
	<div class="success-message">
		{{ Session::get('message') }}
	</div>
@endif