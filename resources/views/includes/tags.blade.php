<div class="tags-container">
	<strong>Теги:</strong>
	@foreach($post->tags as $tag)
		<a href="/tags/{{ $tag->slug }}">{{ $tag->name }}</a>@if(!$loop->last), @endif
	@endforeach
</div>