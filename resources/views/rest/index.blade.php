@extends('layout.master')
@section('title', 'Ресторанная критика')
@section('main_title', 'Ресторанная критика')
@section('mainClass', 'left-h')

@section('content')
	<h1>Ресторанная критика</h1>
	<div style="display:flex;" class="cols reverse">
		<section>
			@foreach($posts as $post)
				@if (Auth::check() || $post->status_ru == 2)
					<div>
						<h3 style="margin-top:0;">
							<a class="{{ $post->status_ru == 1 ? ' hidden-link' : '' }}" href="/rest/{{ $post->slug }}">{{ $post->title_ru }}</a>
						</h3>
					</div>
				@endif
			@endforeach
		</section>
		<aside>
			<p>Иногда я пишу отчеты о посещенных ресторанах, кафе и прочих заведениях общепита.</p>
		</aside>
	</div>
@stop