@extends('layout.master')

@section('title')
	{{ $post->title_ru }}
@stop

@section('main_title')
	<a href="/rest">Ресторанная критика</a>
@stop

@section('styles')
	@parent
	<link rel="stylesheet" type="text/css" href="/css/slideshow.css">
@stop

@section('scripts')
	<script src="//code.jquery.com/jquery-1.12.3.min.js"></script>
	<script src="/js/slideshow.js"></script>
@stop

@section('content')
	<h2 class="post-title">{{ $post->title_ru }}</h2>
	<date class="post-date">{{ full_ru_date($post->visit_date) }}</date>
	<div class="post-body">{!! $post->body_ru !!}</div>

	@if(count($post->comments) > 0)
		<div id="comments">
			<h3>{{ count($post->comments) }} камент(а|ов)</h3>
			<div>
				@foreach ($post->comments as $comment)
					<div>
						<h4>
							{{ $comment->poster_name }}
							<date>{{ full_ru_datetime($comment->date) }}</date>
						</h4>
						<blockquote>
							{!! $comment->body !!}
						</blockquote>
					</div>
				@endforeach
			</div>
		</div>
	@endif
@stop