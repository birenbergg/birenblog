@extends('layout.master')

@section('title')
	[Редактирование]: {{ $post->title }}
@stop

@section('main_title')
	<a href="/zhitie">Житие Шухера</a>
@stop

@section('content')
	<div>
		{{ HTML::ul($errors->all()) }}

		{{ Form::model($post, array('route' => array('zhitie.update', $post->slug), 'method' => 'PUT')) }}

			<div class="edit-block">
				<div>
					{{ Form::label('title', 'Заголовок') }}
					{{ Form::text('title', null) }}
				</div>
			</div>
			<div class="edit-block">
				{{ Form::label('slug', 'Кодовое имя') }}
				{{ Form::text('slug', null) }}
			</div>
			<div class="edit-block">
				{{ Form::label('date', 'Дата публикации') }}
				{{ Form::text('date', null) }}
			</div>
			<div class="edit-block">
				{{ Form::label('status', 'Статус') }}<br />
				{{ Form::select('status', array('1' => 'Черновик', '2' => 'Готовый пост'), null) }}
			</div>
			<div class="edit-block">
				{{ Form::label('body', 'Текст') }}<br />
				{{ Form::textarea('body', null) }}
			</div>

			{{ Form::submit('Сохранить', array('class' => 'save')) }}
		{{ Form::close() }}
		
		{{ Form::open(array('url' => 'zhitie/' . $post->id)) }}
			{{ Form::hidden('_method', 'DELETE') }}
			{{ Form::submit('Удалить', array('class' => 'delete')) }}
		{{ Form::close() }}
	</div>
@stop