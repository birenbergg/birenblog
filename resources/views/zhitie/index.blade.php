@extends('layout.master')
@section('title', 'Житие Шухера')
@section('mainClass', 'blog')

@section('main_title')
	@if ($posts->currentPage() > 1)<a href="/zhitie">@endif
		Житие Шухера
	@if ($posts->currentPage() > 1)</a>@endif
@stop

@section('content')
	<h1>
		@if ($posts->currentPage() > 1)<a href="/zhitie">@endif
			Житие Шухера
		@if ($posts->currentPage() > 1)</a>@endif
	</h1>
	@if(Auth::user() && Auth::user()->id == 1)
		<div class="new-post-button-wrapper">
			<a href="{{ route('zhitie.create') }}">Новый пост</a>
		</div>
	@endif
	<div style="margin-bottom:4em;">
		@if ($posts->currentPage() == 1)
		<div class="epigraph">
			<div>Вы записывайте, записывайте!</div>
			<cite>
				- Шухер
			</cite>
		</div>
		<p id="preface" style="clear: both;" class="hyphenated">
			Было время, мой знакомый друг <a href="//margulo.livejournal.com">margulo</a> вел так называемое "Житие Шухера". Просто так получилось, что ему довелось с Шухером жить и каждый день наслаждаться гениальными фразами. Только успевай записывать.<br />
			<br />
			Потом Марг и Шухер разъехались и ведение "Жития" - этой самой веселой части Маргульского ЖЖ - прекратилось. Но ведь Шухер остался. И фразы его то и дело звучат. А надобность по-любому записывать эти фразы никто не отменял. Что впредь я и собираюсь время от времени делать.<br />
			<br />
			NB: Житие Шухера №№ 1-15 составленно Маргом.
		</p>
		@endif
	</div>
	<div>
		@foreach($posts as $post)
			@if (Auth::check() || $post->status == 2)
				<div class="post{{ $post->status == 1 ? ' hidden-post' : '' }}">
					<h3 class="post-title">
						<a href="/zhitie/{{ $post->slug }}">{{ $post->title }}</a>
						@if(count($post->comments) > 0)
							<div style="text-align:center;color: #ccc;font-weight: normal;font-size: 18px;margin-top: .5em;"><a style="color:#63aac7" href="/zhitie/{{ $post->slug }}#comments">@php echo pluralization(count($post->comments), 'камент камента каментов', true) @endphp</a></div>
						@endif
					</h3>
					<date class="post-date">{{ full_ru_date($post->date) }}</date>
					<div class="post-body">{!! $post->body !!}</div>
				</div>
			@endif
		@endforeach
	</div>
	@if ($posts->lastPage() > 1)
	<div style="text-align: center; font-size: 18px; color: #ccc; margin-top:1em;">
		<div>страница:</div>
		<div style="text-align:center;">
			{!! $posts->render() !!}
		</div>
	</div>
	@endif
@stop