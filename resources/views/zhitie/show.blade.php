@extends('layout.master')

@section('title')
	{{ $post->title }}
@stop

@section('main_title')
	<a href="/zhitie">Житие Шухера</a>
@stop

@section('content')
	<div>
		<h2 class="post-title">{{ $post->title }}</h2>
		<date class="post-date">{{ full_ru_date($post->date) }}</date>

		@if(Auth::user() && Auth::user()->id == 1)
			<div class="admin-buttons">
				<div>
					<a href="/zhitie/{{ $post->slug }}/edit">Редактировать</a>
				</div>
			</div>
		@endif

		<div class="post-body">{!! $post->body !!}</div>

		@if(count($post->comments) > 0)
		<div id="comments">
			<h3>@php echo pluralization(count($post->comments), 'камент камента каментов', true) @endphp</h3>
			<div>
				@foreach ($post->comments as $comment)
					<div>
						<h4>
							{{ $comment->poster_name }}
							<date>{{ full_ru_datetime($comment->date) }}</date>
						</h4>
						<blockquote>
							{!! $comment->body !!}
						</blockquote>
					</div>
				@endforeach
			</div>
		</div>
	</div>
	@endif
@stop