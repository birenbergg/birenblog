@extends('layout.master')
@section('title', 'Теги Бл-га')
@section('mainClass', 'blog')

@section('main_title')
	Теги <a href="/">Бл-га</a>
@stop

@section('content')
	<h1>
		Теги <a href="/">Бл-га</a>
	</h1>
	<ul>
		@foreach($tags as $tag)
			@if(count($tag->posts) > 0)
				<li><a href="/tags/{{ $tag->slug }}">{{ $tag->name }}</a> {{ count($tag->posts) }}</li>
			@endif
		@endforeach
	</ul>
@stop