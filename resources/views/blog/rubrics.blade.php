@extends('layout.master')
@section('title', 'Рубрики Бл-га')
@section('mainClass', 'blog')

@section('main_title')
	Рубрики <a href="/">Бл-га</a>
@stop

@section('content')
	<h1>
		Рубрики <a href="/">Бл-га</a>
	</h1>
	<ul>
		@foreach($rubrics as $rubric)
			@if(count($rubric->posts) > 0)<li><a href="/rubrics/{{ $rubric->slug }}">{{ $rubric->name }}</a> {{ count($rubric->posts) }}</li>@endif
		@endforeach
	</ul>
@stop