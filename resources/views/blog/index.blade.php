@extends('layout.master')

@section('title')
	Бл-г@if ($posts->currentPage() > 1), страница {{ $posts->currentPage() }} @endif
@stop

@section('main_title')
	@if($posts->currentPage() > 1)<a href="/">@endif
	Бл-г
	@if($posts->currentPage() > 1)</a>@endif
@stop

@section('mainClass', 'blog')

@section('content')
	<style type="text/css">
		.expander {
			cursor: pointer;
		}
		h3.post-title a.edit, h1 a.create-new {
			/*display: none;*/
		}

		h3.post-title:hover a.edit, h1:hover a.create-new {
			/*display: inline !important;*/
		}
	</style>
	<h1 style="display: flex; flex-direction: column; align-items: center;">
		@if ($posts->currentPage() > 1 || $rubric || $tag || $year || $month)<a href="/">@endif
		
		Бл-г

		@if ($posts->currentPage() > 1 || $rubric || $tag || $year || $month)</a>@endif

		@if($rubric)
			<div style="font-size: 24px;">
				Посты из рубрики "@if($posts->currentPage() > 1)<a href="/rubrics/{{ $rubric->slug }}">@endif{{ $rubric->name }}@if ($posts->currentPage() > 1)</a>@endif"
			</div>
		@endif

		@if($tag)
			<div style="font-size: 24px;">
				Посты по тегу "{{ $tag->name }}"
			</div>
		@endif

		@if($month)
			<div style="font-size: 24px; font-weight: normal; display: flex; justify-content: center; align-items: center;">
				<span style="font-size: 18px">
					<a href="/{{ $year - ($month == 1) }}/{{ $month == 1 ? 12 : $month - 1 }}">{{ ru_month_nominative($month == 1 ? 12 : $month - 1) }} {{ $month == 1 ? $year - 1 : '' }}</a>
					<span style="display:inline block; margin: 0 1em;">
						&larr;
					</span>
				</span>
				
				<span>
					Посты за {{ ru_month_nominative($month) }} <a href="/{{ $year }}">{{ $year }}</a> года
				</span>

				<span style="font-size: 18px">
					<span style="display:inline block; margin: 0 1em;">
						&rarr;
					</span>
					<a href="/{{ $year + ($month == 12) }}/{{ $month == 12 ? 1 : $month + 1 }}">{{ ru_month_nominative($month == 12 ? 1 : $month + 1) }} {{ $month == 12 ? $year + 1 : '' }}</a>
				</span>
			</div>
		@elseif($year)
			<div style="font-size: 24px; font-weight: normal; display: flex; justify-content: center; align-items: center;">
				@if($year > 2006)
					<span style="font-size: 18px">
						<a href="/{{ $year - 1 }}">{{ $year - 1 }}</a>
						<span style="display:inline block; margin: 0 1em;">
							&larr;
						</span>
					</span>
				@endif
				
				<span>
					Посты за {{ $year }} год
				</span>

				@if($year < 2017)
					<span style="font-size: 18px">
						<span style="display:inline block; margin: 0 1em;">
							&rarr;
						</span>
						<a href="/{{ $year + 1 }}">{{ $year + 1 }}</a>
					</span>
				@endif
			</div>
		@endif

		@if(Auth::user() && Auth::user()->id == 1)
			<a class="create-new" href="{{ route('blog.create') }}" style="text-align: center; display: block; font-size: 16px; font-weight: normal;margin-top: .5em;"><i class="fa fa-lock"></i> Новый пост</a>
		@endif
	</h1>

	@if ($posts->currentPage() == 1 && !$rubric && !$tag && !$year && !$month)
	<div id="preface" style="margin-bottom:4em;">
		<p class="hyphenated">
			В 2006 году я завел себе нетематический ЖЖ. Наши отношения не сложились и я переехал на Блоггер (гугловский сервис). В конце концов природная склонность сделать так, как мне нравится, породила мой собственный блог на моем собственном сайте.
		</p>
		<p class="hyphenated">
			Некоторые посты старого блога, объединенные одной темой, составили целые разделы на сайте (<a href="/outdoors">Поездки</a>, <a href="/rest">Рестораны</a>, <a href="/hrenotive">Хренотив</a>, <a href="/notebook">Записная книжка</a> и <a href="/zhitie">Житие Шухера</a>).
		<p>
		<p>
			Сегодня я все меньше пишу в блог и все больше пишу в <a href="https://www.facebook.com/birenbergg">фейсбук</a>. Во-первых, там удобнее; во-вторых, там во много раз больший фидбек; в-третьих, там можно мусорить как угодно (а избранные посты из фейсбука и так публикуются здесь).
		</p>
		<p>* * *</p>
		<p>NB Мнение автора в {{ date('Y') }}-м году может не совпадать со мнением автора в {{ rand(2006, (date('Y') - 1)) }}-м.</p>
	</div>
	@endif
	
	<div style="display: flex;">
		<div style="width: 73%; width: auto;">
			@if(count($posts))
				@foreach($posts as $post)
					<div class="post{{ $post->status == 1 ? ' hidden-post' : '' }}">
						<div style="display: flex; justify-content: space-between; align-items: flex-start; margin-bottom: 2em;">
							<a href="/{{ $post->slug }}" style="color: #999;">
								<h3 class="post-title" style="font-weight: normal">
									{!! $post->title !!}

									@if($post->rubric && false)
										<div style="text-align:center;color: #ccc;font-weight: normal;font-size: 18px;margin-top: .5em;">Из рубрики "<a style="color:#63aac7" href="/rubrics/{{ $post->rubric->slug }}">{{ $post->rubric->name }}</a>" {{ Auth::check() ? 'rubric id: ' . $post->rubric->id : '' }}</div>
									@endif
								</h3>
							</a>
							<span>
								<date class="post-date" style="margin-bottom: 0">{{ full_ru_datetime($post->date) }}</date>
							</span>
						</div>
						<div class="post-body">{!! $post->body !!}</div>
						@if(count($post->tags) > 0)
							@include('includes.tags')
						@endif

						@if(Auth::user() && Auth::user()->id == 1)
							<a class="edit" href="/blog/{{ $post->slug }}/edit" title="Редактировать"><i class="fa fa-lock"></i> Редактировать</a>
						@endif

						@if(Auth::user() && Auth::user()->id == 1 && count($post->comments) > 0)
						|
						@endif

						<a href="/{{ $post->slug }}#comments">
							@if(count($post->comments) > 0)
								Читать @php echo pluralization(count($post->comments), 'камент камента каментов', true) @endphp или написать свой
							@else
								<!-- <i class="fa fa-comment"></i>
								Написать камент -->
							@endif
						</a>
					</div>
				@endforeach
			@else
				Ничего не найдено.
			@endif
		</div>
		<div style="width: 23%; margin-left: 4em; display: none;">
			<div style="border-top: 1px dotted #999; margin-bottom:2em;">
				<h4>Машина времени</h4>
				<div>
					<ul style="list-style: none; padding: 0;">
					@for ($i = 2017; $i >= 2006; $i--)
						<li>
							<span style="width: 1em; display: inline-block;" id="y{{$i}}-expander" class="expander {{ $i == $year ? 'open' : 'closed' }}" data-list="y{{$i}}-month-list">
								@if($i == $year)
									<i class="fa fa-caret-down"></i>
								@else
									<i class="fa fa-caret-right"></i>
								@endif
							</span>

							@if((!$month && !$year) || $month || $year && !($i == $year))
								<a href="/{{ $i }}">
							@endif

								{{ $i }}

							@if((!$month && !$year) || $month || $year && !($i == $year))
								</a>
							@endif
							<ul style="display: {{ $i == $year ? 'block' : 'none' }}; list-style: none;" id="y{{$i}}-month-list">
								@for ($j = 12; $j >= 1; $j--)
									<li>
										@if((!$month && !$year) || ($year || $month) && !($i == $year && $j == $month))
											<a href="/{{ $i }}/{{ $j }}">
										@endif

											{{ ru_month_nominative($j) }}

										@if((!$month && !$year) || ($year || $month) && !($i == $year && $j == $month))
											</a>
										@endif
									</li>
								@endfor
							</ul>
						</li>
					@endfor
					</ul>
				</div>
			</div>
			<div style="border-top: 1px dotted #999; margin-bottom:2em;">
				<h4><a href="/rubrics">Рубрики</a></h4>
				<ul style="list-style: none; padding: 0;">
					@foreach($rubrics as $rubric)
						<li><a href="/rubrics/{{ $rubric->slug }}">{{ $rubric->name }}</a> ({{ $rubric->posts_count }})</li>
					@endforeach
				</ul>
			</div>
			<div style="border-top: 1px dotted #999; margin-bottom:2em;">
				<h4><a href="/tags">Теги</a></h4>
				<div>
					@foreach($tags as $tag)
						<a style="font-size: {{ $tag->posts_count + 14 }}px" href="/tags/{{ $tag->slug }}">{{ $tag->name }}</a>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	@if ($posts->lastPage() > 1)
	<div style="text-align: center; font-size: 18px; color: #ccc; margin-top:1em;">
		<div>страница:</div>
		<div style="text-align:center;">
			{!! $posts->render() !!}
		</div>
	</div>
	@endif

	<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
	<script type="text/javascript">
		$('.expander').click(function() {
			if ($(this).hasClass('closed')) {
				$('#' + $(this).data('list')).css('display', 'block');
				$(this).removeClass('closed');
				$(this).addClass('open');
				$(this).html('<i class="fa fa-caret-down"></i>');
			} else if($(this).hasClass('open')) {
				$('#' + $(this).data('list')).css('display', 'none');
				$(this).removeClass('open');
				$(this).addClass('closed');
				$(this).html('<i class="fa fa-caret-right"></i>');
			}
		});
	</script>
@stop