@extends('layout.master')

@section('title', 'Новый пост')

@section('main_title')
	<a href="/">Бл-г</a>
@stop

@section('content')
	<div>
		{{ HTML::ul($errors->all()) }}

		{{ Form::open(array('url' => 'blog')) }}
			<div class="edit-block">
				<div>
					{{ Form::label('title', 'Заголовок') }}
					{{ Form::text('title', Input::old('title')) }}
				</div>
			</div>
			<div class="edit-block">
				{{ Form::label('slug', 'Кодовое имя') }}
				{{ Form::text('slug', Input::old('slug')) }}
			</div>
			<div class="edit-block">
				{{ Form::label('body', 'Текст') }}<br />
				{{ Form::textarea('body', Input::old('body')) }}
			</div>
			<div class="edit-block">
				{{ Form::label('status', 'Статус') }}<br />
				{{ Form::select('status', array('2' => 'Готовый пост', '1' => 'Черновик'), Input::old('status')) }}
			</div>
			<div class="edit-block double">
				<div>
					{{ Form::label('custom_date', 'Задать дату публикации вручную') }}
					{{ Form::checkbox('custom_date', null) }}
				</div>
				<div>
					{{ Form::label('date', 'Дата и время публикации') }}
					{{ Form::text('date', null) }}
				</div>
			</div>

			{{ Form::submit('Создать', array('class' => 'save')) }}
		{{ Form::close() }}
	</div>
@stop