@extends('layout.master')

@section('title')
	{{ $post->title }}
@stop

@section('main_title')
	<a href="/">Бл-г</a>
@stop

@section('content')
	<style type="text/css">
		h2.post-title a {
			display: none;
		}

		h2.post-title:hover a {
			display: inline !important;
		}

		#comments h3 {
			text-align: left;
			margin-bottom: 0.5em;
			margin-top: 2em;
		}

		#comments h4 {
			background: #abc;
			margin: 0;
			display: inline;
			border: 0;
			padding: .25em .75em;
			border-radius: 8px 8px 0 0;
		}
		#comments blockquote {
			border: 0;
			background: #abc;
			border-radius: 0 8px 8px 8px;
		}

		#comments .commentus {
			margin-bottom: 2em;
		}
	</style>
	<div>
		<h2 class="post-title">
			{{ $post->title }}
			@if(Auth::user() && Auth::user()->id == 1)
				<a href="/blog/{{ $post->slug }}/edit" title="Редактировать"><i class="fa fa-pencil"></i></a>
			@endif				
		</h2>
		<date class="post-date">{{ full_ru_datetime($post->date) }}</date>

		@if($post->rubric)
		<div style="text-align:center;color: #ccc;font-weight: normal;font-size: 18px;margin-top: .5em;">Из рубрики "<a style="color:#63aac7" href="/rubrics/{{ $post->rubric->slug }}">{{ $post->rubric->name }}</a>" rubric id: {{ Auth::check() ? $post->rubric->id : '' }}</div>
		@endif
		<div class="post-body">{!! $post->body !!}</div>
		@if(count($post->tags) > 0)
			@include('includes.tags')
		@endif

		@if(count($post->comments) > 0)
			<div id="comments">
				<h3>@php echo pluralization(count($post->comments), 'камент камента каментов', true) @endphp</h3>
				@foreach ($post->comments as $comment)
					<div class="commentus">
						<h4>
							{{ $comment->poster_name }}
							<date>{{ full_ru_datetime($comment->date) }}</date>
						</h4>
						<blockquote>
							{!! $comment->body !!}
						</blockquote>
					</div>
				@endforeach
			</div>
		@endif
	</div>
@stop