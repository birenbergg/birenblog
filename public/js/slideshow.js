var main = {
    init: function () {
        this.slideshow.init();
    }
};

main.slideshow = {
    init: function () {
        var images = jQuery('img.slideshow');
        for (var i = 0; i < images.length; i++) {
            if (images[i].className.match(/(^|\s)slideshow($|\s)/)) {
                new this.item(images[i]);
            }
        }
    },

    item: function (image) {
        var layer = document.createElement('ins');

        layer.className = 'slideshow';
        image.parentNode.insertBefore(layer, image);
        var parent = jQuery(layer.appendChild(document.createElement('ins')));
        this.selectors_container = document.createElement('ins');
        layer.firstChild.appendChild(this.selectors_container);
        layer.firstChild.appendChild(image);
        this.image = image;
        var slides = image.onclick();
        var path = image.src.replace(/[^\/]+$/, '');
        this.selectors = [new main.slideshow.selector(this, image.src, 0)];
        for (i = 0; i < slides.length; i++) {
            this.selectors[this.selectors.length] = new main.slideshow.selector(this, (slides[i].indexOf('/') < 0 ? path : '') + slides[i], i + 1);
        }
        this.selectors[0].container.className = 'selected';
        this.i = 0;
        var t = this;
        // 31.05
        for (i = 0; i < slides.length; i++) {
            main.slideshow.append_dummy(parent, slides[i], t);
        }
        image.onclick = function (oEvent) {
            main.slideshow.image_click(oEvent, t);
        }
    },

    append_dummy: function (parent, name, item) {
        var molly = jQuery(parent.find('img.slideshow')[0]);
        var dummy = molly.clone();
        dummy.attr('src-dummy', name);
        dummy.attr('src', '');
        dummy.hide();
        parent.append(dummy);
        dummy.click(function (oEvent) {
            main.slideshow.image_click(oEvent, item);
        });
    },

    give_dummy_life: function (parent, imageName) {
        var element = jQuery(parent.find('img.slideshow[src-dummy*="' + imageName + '"]'));
        var imageSrc = element.attr('src-dummy');
        element.attr('src', imageSrc);
        element.removeAttr('src-dummy');
    },

    selector: function (item, src, i) {
        this.container = document.createElement('ins');
        item.selectors_container.appendChild(this.container);
        var t = this;
        var parent = jQuery(item.image).closest('div.image');
        this.container.onclick = function () {
            t.select();
        };
        this.select = function () {
            item.selectors[item.i].container.className = '';
            t.container.className = 'selected';
            item.i = i;
            // 31.05
            // item.image.src = src;
            t.deside_append_and_show();
        };

        // 31.05
        this.deside_append_and_show = function () {

            var imageName = src.match(/[\w\.]*$/)[0];
            var append = false;

            var deside = parent.find('img.slideshow[src*="' + imageName + '"]');

            if (deside.length == 0) {
                main.slideshow.give_dummy_life(parent, imageName);
            }

            parent.find('img.slideshow').hide();
            parent.find('img.slideshow[src*="' + imageName + '"]').show();

        }
    },
    // 31.05
    image_click: function (oEvent, t) {
        document.cookie = 'i_like_slideshow=1; path=/; expires=Mon, 01-Jan-2020 00:00:00 GMT';
        if (!oEvent)
            var oEvent = window.event;

        if (oEvent.shiftKey) {
            t.selectors[t.i == 0 ? t.selectors.length - 1 : t.i - 1].select();
        } else {
            t.selectors[t.i >= t.selectors.length - 1 ? 0 : t.i + 1].select();
        }
    }
};

main.init();

(function () {
    var isLike = document.cookie.match(/i_like_slideshow=(\d)/);

    if (isLike == null) {

        return;

    } else {

        if (isLike.length > 0 && isLike[1] == 1) {
            var dummies = jQuery('img.slideshow[src-dummy]');
            dummies.each(function () {
                var thisElement = jQuery(this);
                var imageName = thisElement.attr('src-dummy');
                var parent = thisElement.closest('div.image');
                main.slideshow.give_dummy_life(parent, imageName);
            });
        }

    }
}());