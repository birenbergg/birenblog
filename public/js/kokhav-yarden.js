$('#israel').hover(function () {
    $('img#all').hide();
    $('img#israel-highlighted').show();
    $('img#jordan-highlighted').hide();
    $('img#golan-highlighted').hide();
})
$('#jordan').hover(function () {
    $('img#all').hide();
    $('img#israel-highlighted').hide();
    $('img#jordan-highlighted').show();
    $('img#golan-highlighted').hide();
})
$('#golan').hover(function () {
    $('img#all').hide();
    $('img#israel-highlighted').hide();
    $('img#jordan-highlighted').hide();
    $('img#golan-highlighted').show();
})
$('#israel, #jordan, #golan').mouseleave(function () {
    $('img#all').show();
    $('img#israel-highlighted').hide();
    $('img#jordan-highlighted').hide();
    $('img#golan-highlighted').hide();
})