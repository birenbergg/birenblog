(function(){

	angular.module("Blog").controller("blogController", ['$scope', '$http', function ($scope, $http) {
		$scope.loadingPostData = true;
		$scope.selectedMode = 'view';

		$scope.getPostToEdit = function(slug) {
			if (slug == null) {
				$scope.post = new {
					title: 'test title',
					body: 'test body',
					date: '',
					status :0

				};
				$scope.loadingPostData = false;
			} else {
				$http.get("/api/blog/" + slug).then(function(result) {
					$scope.post = result.data;
				}).then(function() {
					$scope.loadingPostData = false;
				});
			}
		}

		$scope.savePost = function(neww) {
			if (neww) {
				$http({
					url: "/api/blog/",
					params: {
						date: new Date().toISOString(),
						title: $scope.post.title,
						body: $scope.post.body,
						status: 2
					},
					method: 'put',
					headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
				}).then(function() {
					// do some more stuff
				});
				$scope.post = {
					title: '',
					date: new Date().toISOString(),
					id: null,
					body
				}
			} else {
				$http({
					url: "/api/blog/" + $scope.post.id,
					params: {
						date: $scope.post.date,
						title: $scope.post.title,
						body: $scope.post.body
					},
					method: 'POST',
					headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
				}).then(function() {
					// do some more stuff
				});
			}
		}
			
	}]);

}());