<?php

Route::resource('blog', 'BlogController');
Route::resource('outdoors', 'OutdoorsController');
Route::resource('rest', 'RestController');
Route::resource('hrenotive', 'HrenotiveController');
Route::resource('notebook', 'NotebookController');
Route::resource('zhitie', 'ZhitieController');


Route::get('/{year}', 'BlogController@getPostsByYear')->where('year', '[0-9]+');
Route::get('/{year}/{month}', 'BlogController@getPostsByMonth')->where('year', '[0-9]+');

// Other

Route::get('/miskhakische-logo', function () {
	return typograf(view('miskhakische-logo')->with('langs'));
})->name('miskhakische-logo');


// Utils

Route::get('/login',function () {
	return view('login');
});

Route::post('/register',[
	'uses' => 'UserController@postRegister',
	'as' => 'register'
]);

Route::post('/signin',[
	'uses' => 'UserController@postSignIn',
	'as' => 'signin'
]);

Route::get('/logout',[
	'uses' => 'UserController@getLogout',
	'as' => 'logout'
]);



// Blog

Route::get('/rubrics', 'BlogController@getRubrics')->name('rubrics');
Route::get('/rubrics/{rubricSlug}', 'BlogController@getPostsByRubric')->name('blog_posts_by_rubric');

Route::get('/tags', 'BlogController@getTags')->name('tags');
Route::get('/tags/{tagName}', 'BlogController@getPostsByTag')->name('blog_posts_by_tag');

Route::get('/new-post', 'BlogController@newPost');

Route::get('/api/blog/{slug}', 'BlogController@getPostData');

Route::get('/api/outdoors/{slug}', 'OutdoorsController@getPostData');

Route::post('/api/blog/{slug}', 'BlogController@savePostData');

Route::get('/edit/{slug}', 'BlogController@edit')->name('blog.edit');
Route::get('/{slug}', 'BlogController@show')->name('blog.show');

Route::get('/', 'BlogController@index')->name('blog.index');
