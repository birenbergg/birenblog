<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutdoorsPost extends Model
{
	public $timestamps = false;

	public function components() {
		return $this->hasMany('App\OutdoorsPostComponent', 'post_id')->orderBy('index');
	}

	public function comments() {
		return $this->hasMany('App\OutdoorsComment', 'post_id')->orderBy('date');
	}
}