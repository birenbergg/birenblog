<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutdoorsSlideshowImage extends Model
{
	protected $table = 'outdoors_slideshow_images';
}