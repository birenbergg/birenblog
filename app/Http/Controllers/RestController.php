<?php

namespace App\Http\Controllers;

use App;
use App\RestPost;

class RestController extends Controller
{
	public function index()
	{
		$posts = RestPost::orderBy('date', 'desc')->get();
		return view('rest.index', ['posts' => $posts]);
	}

	public function create()
    {
        //
    }

    public function store()
    {
        //
    }

	public function show($slug)
	{
		include(app_path() . '/functions/datetime_formats.php');		
		$post = RestPost::where('slug', $slug)->first();
		return view('rest.show', ['post' => $post]);
	}

	public function edit($slug)
	{
		//
	}

	public function update($id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}