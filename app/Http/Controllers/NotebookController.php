<?php

namespace App\Http\Controllers;

use App\Note;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class NotebookController extends Controller
{
	public function index()
	{
		$notes = Note::orderBy('id', 'desc')->get();
		return view('notebook.index', ['notes' => $notes]);
	}

	public function create()
    {
    	if (Auth::user() && Auth::user()->id == 1) {
        	return view('notebook.create');
        } else {
        	return view('login');
        }
    }

    public function store()
    {
    	if (Auth::user() && Auth::user()->id == 1) {
	        $rules = array(
	            'body' => 'required',
	        );

	        $validator = Validator::make(Input::all(), $rules);

	        // process the login
	        if ($validator->fails()) {
	            return Redirect::to('notebook/create')
	                ->withErrors($validator)
	                ->withInput(Input::except('password'));
	        } else {
	            // store
	            $note = new Note;
	            $note->body = Input::get('body');
	            $note->save();

	            // redirect
	            return Redirect::to('notebook');
	        }
    	} else {
        	return view('login');
        }
    }

	public function edit($id)
	{	if (Auth::user() && Auth::user()->id == 1) {
			$note = Note::where('id', $id)->first();
			return view('notebook.edit', ['note' => $note]);
		} else {
        	return view('login');
        }
	}

	public function update($id)
    {
    	if (Auth::user() && Auth::user()->id == 1) {
	        $rules = array(
	            'body' => 'required',
	        );

	        $validator = Validator::make(Input::all(), $rules);

	        // process the login
	        if ($validator->fails()) {
	            return Redirect::to('notebook/create')
	                ->withErrors($validator)
	                ->withInput(Input::except('password'));
	        } else {
	            // store
	            $note = Note::find($id);
	            $note->body = Input::get('body');
	            $note->update();

	            // redirect
	            return Redirect::to('notebook');
	        }
    	} else {
        	return view('login');
        }
    }

    public function destroy($id)
    {
    	if (Auth::user() && Auth::user()->id == 1) {
	        // delete
	        $post = Note::find($id);
	        $post->delete();

	        // redirect
	        return Redirect::to('notebook');
    	} else {
        	return view('login');
        }
    }
}