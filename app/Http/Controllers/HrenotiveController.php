<?php

namespace App\Http\Controllers;

use App\HrenotivePost;
use Auth;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class HrenotiveController extends Controller
{
	public function index()
	{
		$hrenotives = HrenotivePost::orderBy('date', 'desc')->get();
		return view('hrenotive.index', ['hrenotives' => $hrenotives]);
	}

	public function create()
    {
        //
    }

    public function store()
    {
        //
    }

	public function show($slug)
	{
		include(app_path() . '/functions/datetime_formats.php');

		$hrenotive = HrenotivePost::where('slug', $slug)->first();
		return view('hrenotive.show', ['hrenotive' => $hrenotive]);
	}

	public function edit($slug)
	{
		//
	}

	public function update($id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}