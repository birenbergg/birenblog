<?php

namespace App\Http\Controllers;

use Auth;
use LengthAwarePaginator;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\ZhitiePost;

class ZhitieController extends Controller
{
	public function index()
	{
		include(app_path() . '/functions/datetime_formats.php');

		$posts = ZhitiePost::orderBy('date', 'desc')->paginate(300);
		return view('zhitie.index', ['posts' => $posts]);
	}

	public function create()
    {
    	if (Auth::user() && Auth::user()->id == 1) {
        	return view('zhitie.create');
        } else {
        	return view('login');
        }
    }

    public function store()
    {
    	if (Auth::user() && Auth::user()->id == 1) {
	        $rules = array(
	            'title' => 'required',
	            'slug' => 'required|unique:zhitie_posts',
	            'body' => 'required',
	        );

	        $validator = Validator::make(Input::all(), $rules);

	        // process the login
	        if ($validator->fails()) {
	            return Redirect::to('zhitie/create')
	                ->withErrors($validator)
	                ->withInput(Input::except('password'));
	        } else {
	            // store
	            $post = new ZhitiePost;

	            $post->title = Input::get('title');
	            $post->slug = Input::get('slug');
	            $post->date = Input::get('custom_date') ? Input::get('date') : date('Y-m-d H:i:s');
	            $post->status = Input::get('status');
	            $post->body = Input::get('body');

	            $post->save();

	            // redirect
	            return Redirect::to('zhitie');
	        }
    	} else {
        	return view('login');
        }
    }

	public function show($slug)
	{
		include(app_path() . '/functions/datetime_formats.php');
		$post = ZhitiePost::where('slug', $slug)->firstOrFail();
		return view('zhitie.show', ['post' => $post]);
	}

	public function edit($slug)
	{
		if (Auth::user() && Auth::user()->id == 1) {
			$post = ZhitiePost::where('slug', $slug)->firstOrFail();
			return view('zhitie.edit', ['post' => $post]);
    	} else {
        	return view('login');
        }
	}

	public function update($slug)
    {
    	if (Auth::user() && Auth::user()->id == 1) {
	        $rules = array(
	            'title' => 'required',
	            'slug' => 'required',
	            'date' => 'required',
	            'body' => 'required',
	        );
	        $validator = Validator::make(Input::all(), $rules);

	        // process the login
	        if ($validator->fails()) {
	            return Redirect::to('zhitie/' . $slug . '/edit')
	                ->withErrors($validator)
	                ->withInput(Input::except('password'));
	        } else {
	            // store
				$post = ZhitiePost::where('slug', $slug)->firstOrFail();

	            $post->title = Input::get('title');
	            $post->slug = Input::get('slug');
	            $post->date = Input::get('date');
	            $post->status = Input::get('status');
	            $post->body = Input::get('body');
	            
	            $post->update();

	            // redirect
	            return Redirect::to('zhitie');
	        }
    	} else {
        	return view('login');
        }
    }

    public function destroy($id)
    {
    	if (Auth::user() && Auth::user()->id == 1) {
	        // delete
	        $post = ZhitiePost::find($id);
	        $post->delete();

	        // redirect
	        return Redirect::to('zhitie');
    	} else {
        	return view('login');
        }
    }
}