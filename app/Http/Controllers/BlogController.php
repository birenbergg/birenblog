<?php

namespace App\Http\Controllers;

use Auth;
use LengthAwarePaginator;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\BlogPost;
use App\BlogRubric;
use App\BlogTag;

use Carbon\Carbon;

class BlogController extends Controller
{
	public function typ($str) {
		// TODO: change 

		$new_string = str_replace(" - ", "&nbsp;&mdash; ", $str);


		// TODO: quotes, quotes inside quotes, joint small words, joint numbers, joint abbreviaions

		// $new_string = str_replace(" \"", " &laquo", $new_string);
		// $new_string = str_replace("\" ", "&raquo ", $new_string);

		return $new_string;
	}

	public function parser($str) {

		$str = preg_replace('/"(.*?)"/', '&laquo;$1&raquo;', $str);
		$str = preg_replace('/\*\*(.*?)\*\*/', '<strong>$1</strong>', $str);
		// $str = preg_replace('/\/(.*?)\//', '<em>$1</em>', $str);
		
		$str = preg_replace('/^/', '<p>', $str);
		$str = preg_replace('/$/', '</p>', $str);

		$str = preg_replace('/\r\n\r\n/', '</p><p>', $str);

		$str = preg_replace('/\r\n/', '<br />', $str);

		$str = preg_replace('/---/', '&mdash;', $str);
		$str = preg_replace('/--/', '&ndash;', $str);

		$str = preg_replace('/\[img\s([-a-zA-Z0-9]+)\s(\d+)\]/', '<img src="/images/blog/$1/$2.jpg" />', $str);
		$str = preg_replace('/<p>(\<img.*?>)<\/p>/', '$1', $str);

		$str = preg_replace('/(\bи\b\s)/', '$1&nbsp;', $str);

		return $str;
	}

	public function index()
	{
		include(app_path() . '/functions/datetime_formats.php');

		$posts = BlogPost::where('status', Auth::check() ? '>' : '=', Auth::check() ? 0 : 2)
			->orderBy('date', 'desc')
			->paginate(50);

		// $postsByYears = BlogPost::where('status', Auth::check() ? '>' : '=', Auth::check() ? 0 : 2)
		// 	->orderBy('date', 'desc')->get();

		$rubrics = BlogRubric::orderBy('name')->withCount('posts')->get();
		$tags = BlogTag::orderBy('name')->withCount('posts')->get();

		for ($i = 0; $i < count($posts); $i++) { 
			if($posts[$i]->experimental) {
				$posts[$i]->body = $this->parser($posts[$i]->body);
			}
		}

		return $this->typ(view('blog.index', ['posts' => $posts, 'rubric' => null, 'tag' => null, 'rubrics' => $rubrics, 'tags' => $tags, 'year' => null, 'month' => null]));
		// return $postsByYears;
	}

	public function create()
    {
    	if (Auth::user() && Auth::user()->id == 1) {
        	return view('blog.create');
        } else {
        	return view('login');
        }
    }

    public function store()
    {
    	if (Auth::user() && Auth::user()->id == 1) {
	        $rules = array(
	            'title' => 'required',
	            'slug' => 'required|unique:blog_posts',
	            'body' => 'required',
	        );

	        $validator = Validator::make(Input::all(), $rules);

	        // process the login
	        if ($validator->fails()) {
	            return Redirect::to('blog/create')
	                ->withErrors($validator)
	                ->withInput(Input::except('password'));
	        } else {
	            // store
	            $post = new BlogPost;

	            $post->title = Input::get('title');
	            $post->slug = Input::get('slug');
	            $post->date = Input::get('custom_date') ? Input::get('date') : date('Y-m-d H:i:s');
	            $post->status = Input::get('status');
	            $post->body = Input::get('body');
	            $post->experimental = 1;

	            $post->save();

	            // redirect
	            return Redirect::to('blog');
	        }
    	} else {
        	return view('login');
        }
    }

	public function show($slug)
	{
		include(app_path() . '/functions/datetime_formats.php');
		$post = BlogPost::where('slug', $slug)->firstOrFail();
		return view('blog.show', ['post' => $post]);
	}

	public function edit($slug)
	{
		if (Auth::user() && Auth::user()->id == 1) {
			$post = BlogPost::where('slug', $slug)->firstOrFail();
			return view('blog.edit', ['post' => $post]);
    	} else {
        	return view('login');
        }
	}

	public function update($slug)
    {
    	if (Auth::user() && Auth::user()->id == 1) {
	        $rules = array(
	            'title' => 'required',
	            'slug' => 'required',
	            'date' => 'required',
	            'body' => 'required',
	        );
	        $validator = Validator::make(Input::all(), $rules);

	        // process the login
	        if ($validator->fails()) {
	            return Redirect::to('blog/' . $slug . '/edit')
	                ->withErrors($validator)
	                ->withInput(Input::except('password'));
	        } else {
	            // store
				$post = BlogPost::where('slug', $slug)->firstOrFail();

	            $post->title = Input::get('title');
	            $post->slug = Input::get('slug');
	            $post->date = Input::get('date');
	            $post->status = Input::get('status');
	            $post->body = Input::get('body');
	            
	            $post->update();

	            // redirect
	            return Redirect::to('blog');
	        }
    	} else {
        	return view('login');
        }
    }

    public function destroy($id)
    {
    	if (Auth::user() && Auth::user()->id == 1) {
	        // delete
	        $post = BlogPost::find($id);
	        $post->delete();

	        // redirect
	        return Redirect::to('blog');
    	} else {
        	return view('login');
        }
    }

    //////////////////////

	public function getPostsByRubric($rubricSlug)
	{
		include(app_path() . '/functions/datetime_formats.php');

		$rubrics = BlogRubric::orderBy('name')->withCount('posts')->get();
		$tags = BlogTag::orderBy('name')->withCount('posts')->get();

		$rubric = BlogRubric::where('slug', $rubricSlug)->first();
		$posts = BlogPost::where('rubric_id', $rubric->id)
			->where('status', Auth::check() ? '>' : '=', Auth::check() ? 0 : 2)
			->orderBy('date', 'desc')
			->paginate(50);

		return view('blog.index', ['rubric' => $rubric, 'tag' => null, 'posts' => $posts, 'month' => null, 'rubrics' => $rubrics, 'tags' => $tags, 'year' => null]);
	}

	public function getPostsByTag($tagName)
	{
		include(app_path() . '/functions/datetime_formats.php');

		$rubrics = BlogRubric::orderBy('name')->withCount('posts')->get();
		$tags = BlogTag::orderBy('name')->withCount('posts')->get();

		$tag = BlogTag::where('slug', $tagName)->first();
		$posts = BlogPost::where('rubric_id', $rubric->id)
			->where('status', Auth::check() ? '>' : '=', Auth::check() ? 0 : 2)
			->orderBy('date', 'desc')
			->paginate(50);

		return view('blog.index', ['rubric' => null, 'tag' => $tag, 'posts' => $posts, 'month' => null, 'rubrics' => $rubrics, 'tags' => $tags, 'year' => null]);
	}

	public function getPostsByYear($year)
	{
		include(app_path() . '/functions/datetime_formats.php');

		$rubrics = BlogRubric::orderBy('name')->withCount('posts')->get();
		$tags = BlogTag::orderBy('name')->withCount('posts')->get();

		$posts = BlogPost::whereYear('date', '=', $year)
			->where('status', Auth::check() ? '>' : '=', Auth::check() ? 0 : 2)
			->orderBy('date', 'desc')
			->paginate(50);

		return view('blog.index', ['posts' => $posts, 'rubric' => null, 'tag' => null, 'rubrics' => $rubrics, 'tags' => $tags, 'year' => $year, 'month' => null]);
	}

	public function getPostsByMonth($year, $month)
	{
		include(app_path() . '/functions/datetime_formats.php');
		
		$rubrics = BlogRubric::orderBy('name')->withCount('posts')->get();
		$tags = BlogTag::orderBy('name')->withCount('posts')->get();

		$posts = BlogPost::whereYear('date', '=', $year)->whereMonth('date', '=', $month)
			->where('status', Auth::check() ? '>' : '=', Auth::check() ? 0 : 2)
			->orderBy('date', 'desc')
			->paginate(50);

		return view('blog.index', ['posts' => $posts, 'rubric' => null, 'tag' => null, 'rubrics' => $rubrics, 'tags' => $tags, 'year' => $year, 'month' => $month]);
	}

	// API
	public function getPostData($slug)
	{
		return BlogPost::where('slug', $slug)->with('comments')->firstOrFail();
	}

	public function savePostData(Request $request, $id)
	{
		$post = BlogPost::find($id);

        $post->title = $request->input('title');
        $post->date = $request->input('date');
        $post->body = $request->input('body');
        
        $post->save();
	}

    public function createPost(Request $request) {
        $post = new BlogPost;

        $post->title = $request->input('title');
        $post->date = $request->input('date');
        $post->body = $request->input('body');
        $post->status = $request->input('status');

        $post->save();
    }

	public function getRubrics()
	{
		include(app_path() . '/functions/datetime_formats.php');
		$rubrics = BlogRubric::orderBy('name')->get();
		return view('blog.rubrics', ['rubrics' => $rubrics]);
	}

	public function getTags()
	{
		include(app_path() . '/functions/datetime_formats.php');
		$tags = BlogTag::orderBy('name')->get();
		return view('blog.tags', ['tags' => $tags]);
	}
}