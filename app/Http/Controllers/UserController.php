<?php

namespace App\Http\Controllers;

use App;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
	public function postRegister(Request $request)
	{
		$this->validate($request, [
			'username' => 'required',
			'password' => 'required'
		]);

		$username = $request['username'];
		$password = bcrypt($request['password']);

		$user = new User();
		
		$user->username = $username;
		$user->password = $password;

		$user->save();

		Auth::login($user);

		return redirect()->route('blog.index');
	}

	public function postSignIn(Request $request)
	{
		$this->validate($request, [
			'username' => 'required',
			'password' => 'required'
		]);

		if(Auth::attempt(['username' => $request['username'], 'password' => $request['password']], true)) {
			return redirect()->route('blog.index');
		}
		return redirect()->back();
	}

	public function getLogout()
	{
		Auth::logout();
		return redirect()->route('blog.index');
	}
}