<?php

namespace App\Http\Controllers;

use App\OutdoorsPost;
use Auth;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class OutdoorsController extends Controller
{
	public function index()
	{
		include(app_path() . '/functions/datetime_formats.php');
		$data = OutdoorsPost::orderBy('year_start', 'desc')->orderBy('month_start', 'desc')->orderBy('id', 'desc')->orderBy('travel_date_start', 'desc')->get()->groupBy('year_start');

		return view('outdoors.index', ['data' => $data]);
		// return $data;
	}

	public function create()
    {
    	if (Auth::user() && Auth::user()->id == 1) {
        	return view('outdoors.create');
        } else {
        	return view('login');
        }
    }

    public function store()
    {
    	if (Auth::user() && Auth::user()->id == 1) {
	        $rules = array(
	            'title' => 'required',
	            'slug' => 'required|unique:outdoors_posts',
	            'travel_date_start' => 'required|date',
	            'year_start' => 'required|numeric',
	            'month_start' => 'required|numeric',
	            'status' => 'required|numeric',
	        );

	        $validator = Validator::make(Input::all(), $rules);

	        // process the login
	        if ($validator->fails()) {
	            return Redirect::to('outdoors/create')
	                ->withErrors($validator)
	                ->withInput(Input::except('password'));
	        } else {
	            // store
	            $post = new OutdoorsPost;

	            $post->title = Input::get('title');
	            $post->list_title = Input::get('list_title');
	            
	            $post->slug = Input::get('slug');
	            
	            $post->travel_date_start = Input::get('travel_date_start');
	            $post->travel_date_end = Input::get('travel_date_end');
	            
	            $post->year_start = Input::get('year_start');
	            $post->year_end = Input::get('year_end');

	            $post->month_start = Input::get('month_start');
	            $post->month_end = Input::get('month_end');

	            $post->date = Input::get('custom_date') ? Input::get('date') : date('Y-m-d H:i:s');
	            
	            $post->status = Input::get('status');

	            $post->place_id = 0;

	            $post->has_components = 0;

	            $post->custom_sequel_slug = Input::get('custom_sequel_slug');

	            $post->body = Input::get('body');

	            $post->part_number = Input::get('part_number');
	            $post->parts_number = Input::get('parts_number');

	            $post->slug_without_part_number = Input::get('slug_without_part_number');

	            $post->sequel_ready = Input::get('sequel_ready');

	            $post->save();

	            // redirect
	            return Redirect::to('outdoors');
	        }
    	} else {
        	return view('login');
        }
    }

	public function show($slug)
	{
		include(app_path() . '/functions/datetime_formats.php');
		$post = OutdoorsPost::where('slug', $slug)->firstOrFail();
		return view('outdoors.show', ['post' => $post]);
	}

	public function edit($slug)
	{
		if (Auth::user() && Auth::user()->id == 1) {
			$post = OutdoorsPost::where('slug', $slug)->firstOrFail();
			return view('outdoors.edit', ['post' => $post]);
    	} else {
        	return view('login');
        }
	}

	public function update($id)
    {
		if (Auth::user() && Auth::user()->id == 1) {
        	$rules = array(
	            'title' => 'required',
	            'slug' => 'required',
	            'travel_date_start' => 'required|date',
	            'year_start' => 'required|numeric',
	            'month_start' => 'required|numeric',
	            'status' => 'required|numeric',
	        );

	        $validator = Validator::make(Input::all(), $rules);

	        // process the login
	        if ($validator->fails()) {
	            return Redirect::to('outdoors/' . $id . '/edit')
	                ->withErrors($validator)
	                ->withInput(Input::except('password'));
	        } else {
	            // store
	            $post = OutdoorsPost::find($id);

	            $post->title = Input::get('title');
	            $post->list_title = Input::get('list_title');
	            
	            $post->slug = Input::get('slug');
	            
	            $post->travel_date_start = Input::get('travel_date_start');
	            $post->travel_date_end = Input::get('travel_date_end');
	            
	            $post->year_start = Input::get('year_start');
	            $post->year_end = Input::get('year_end');

	            $post->month_start = Input::get('month_start');
	            $post->month_end = Input::get('month_end');

	            $post->date = Input::get('custom_date') ? Input::get('date') : date('Y-m-d H:i:s');
	            
	            $post->status = Input::get('status');

	            $post->place_id = 0;

	            $post->has_components = 0;

	            $post->custom_sequel_slug = Input::get('custom_sequel_slug');

	            $post->body = Input::get('body');

	            $post->part_number = Input::get('part_number');
	            $post->parts_number = Input::get('parts_number');

	            $post->slug_without_part_number = Input::get('slug_without_part_number');

	            $post->sequel_ready = Input::get('sequel_ready');

	            $post->update();

	            // redirect
	            return Redirect::to('outdoors');
	        }
        } else {
        	return view('login');
        }
    }

    public function destroy($id)
    {
    	if (Auth::user() && Auth::user()->id == 1) {
	        // delete
	        $post = OutdoorsPost::find($id);
	        $post->delete();

	        // redirect
	        return Redirect::to('outdoors');
    	} else {
        	return view('login');
        }
    }


	// API
	public function getPostData($slug)
	{
		include(app_path() . '/functions/datetime_formats.php');

		return OutdoorsPost::where('slug', $slug)->with('components.slideshow_images')->first();
	}
}