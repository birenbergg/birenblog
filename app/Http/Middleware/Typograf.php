<?php

namespace App\Http\Middleware;

use Closure;

class Typograf
{
	public function handle($request, Closure $next)
	{
		include(app_path() . '/functions/remotetypograf.php');
		include(app_path() . '/functions/language_tools.php');
		
		return $next($request);
	}
}
