<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTag extends Model
{
	public function posts() {
		return $this->belongsToMany('App\BlogPost', 'blog_tag_to_post', 'tag_id', 'post_id');
	}
}