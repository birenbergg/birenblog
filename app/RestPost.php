<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestPost extends Model
{
	public function comments() {
		return $this->hasMany('App\RestComment', 'post_id')->orderBy('date');
	}
}