<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestComment extends Model
{
	public $timestamps = false;
	protected $table = 'rest_comments';
}