<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZhitiePost extends Model
{
	public $timestamps = false;

	public function comments() {
		return $this->hasMany('App\ZhitieComment', 'post_id')->orderBy('date');
	}
}