<?php

function year($date){
	return date("Y", strtotime($date));
}

function month_nominative($month_number, $locale) {
    switch ($locale) {
        case 'en': return en_month($month_number);
        case 'ru': return ru_month_nominative($month_number);
        case 'he': return he_month_nominative($month_number);
        case 'uk': return uk_month_nominative($month_number);
    }
}

function date_range_without_year($date1, $date2, $locale) {
    $day1    = date('j', strtotime($date1));
    $month1  = date('m', strtotime($date1));
    $year1   = date('Y', strtotime($date1));

    $day2    = date('j', strtotime($date2));
    $month2  = date('m', strtotime($date2));
    $year2   = date('Y', strtotime($date2));

    if ($date1 != null) {
        if ($date2 == null) { // fix
            switch ($locale) {
                case 'en': return en_date($date1);
                case 'ru': return ru_date($date1);
                case 'he': return he_date($date1);
                case 'uk': return uk_date($date1);
            }
        } else if ($year1 !== $year2) {
            switch ($locale) {
                case 'en': return full_en_date($date1) . ' &mdash; ' . full_en_date($date2);
                case 'ru': return full_ru_date($date1) . ' &mdash; ' . full_ru_date($date2);
                case 'he': return full_he_date($date1) . ' &mdash; ' . full_he_date($date2);
                case 'uk': return full_uk_date($date1) . ' &mdash; ' . full_uk_date($date2);
            }
        } else if ($month1 === $month2) {
            switch ($locale) {
                case 'en': return en_month($month1) . ' ' . $day1 . '&mdash;' . $day2;
                case 'ru': return $day1 . '&mdash;' . $day2 . ' ' . ru_month_genetive($month1);
                case 'he': return $day1 . '&mdash;' . $day2 . ' ' . he_month_genetive($month1);
                case 'uk': return $day1 . '&mdash;' . $day2 . ' ' . uk_month_genetive($month1);
            }
        } else {
            switch ($locale) {
                case 'en': return en_month($month1) . ' ' . $day1 . ' &mdash; ' . en_month($month2) . ' ' . $day2;
                case 'ru': return $day1 . ' ' . ru_month_genetive($month1) . ' &mdash; ' . $day2 . ' ' . ru_month_genetive($month2);
                case 'he': return $day1 . ' ' . he_month_genetive($month1) . ' &mdash; ' . $day2 . ' ' . he_month_genetive($month2);
                case 'uk': return $day1 . ' ' . uk_month_genetive($month1) . ' &mdash; ' . $day2 . ' ' . uk_month_genetive($month2);
            }
        }
}

}

// RU

function date_range($date1, $date2) {
    $day1    = date('j', strtotime($date1));
    $month1  = date('m', strtotime($date1));
    $year1   = date('Y', strtotime($date1));

    $day2    = date('j', strtotime($date2));
    $month2  = date('m', strtotime($date2));
    $year2   = date('Y', strtotime($date2));

    if ($date2 == null) { // fix
        return full_ru_date($date1);
    } else if ($year1 !== $year2) {
        return full_ru_date($date1) . ' &mdash; ' . full_ru_date($date2);
    } else if ($month1 === $month2) {
        return $day1 . '&mdash;' . $day2 . ' ' . ru_month_genetive($month1) . ' ' . $year1;
    } else {
        return $day1 . ' ' . ru_month_genetive($month1) . ' &mdash; ' . $day2 . ' ' . ru_month_genetive($month2) . ' ' . $year1;
    }

}

function ru_date($date) {
    $day    = date('j ', strtotime($date));
    $month  = date('m', strtotime($date));

    return  $day . ru_month_genetive($month);
}

function full_ru_date($date) {
	$day	= date('j ', strtotime($date));
	$month	= date('m', strtotime($date));
	$year	= date(' Y', strtotime($date));

	return	$day .
			ru_month_genetive($month) .
			$year;
}

function full_ru_datetime($datetime) {
	$day	= date('j ', strtotime($datetime));
	$month	= date('m', strtotime($datetime));
	$year	= date(' Y, ', strtotime($datetime));
	$time	= date('H:i', strtotime($datetime));

	return	$day .
			ru_month_genetive($month) . 
			$year . 
			$time;
}

function ru_month_nominative($month_number) {
    switch ($month_number) {
        case 1:     return "январь";
        case 2:     return "февраль";
        case 3:     return "март";
        case 4:     return "апрель";
        case 5:     return "май";
        case 6:     return "июнь";
        case 7:     return "июль";
        case 8:     return "август";
        case 9:     return "сентябрь";
        case 10:    return "октябрь";
        case 11:    return "ноябрь";
        case 12:    return "декабрь";
    }
}

function ru_month_short($month_number) {
    switch ($month_number) {
        case 1:     return "янв.";
        case 2:     return "фев.";
        case 3:     return "март";
        case 4:     return "апр.";
        case 5:     return "май";
        case 6:     return "июнь";
        case 7:     return "июль";
        case 8:     return "авг.";
        case 9:     return "сент.";
        case 10:    return "окт.";
        case 11:    return "нояб.";
        case 12:    return "дек.";
    }
}

function ru_month_genetive($month_number) {
    switch ($month_number) {
        case 1:     return "января";
        case 2:     return "февраля";
        case 3:     return "марта";
        case 4:     return "апреля";
        case 5:     return "мая";
        case 6:     return "июня";
        case 7:     return "июля";
        case 8:     return "августа";
        case 9:     return "сентября";
        case 10:    return "октября";
        case 11:    return "ноября";
        case 12:    return "декабря";
    }
}


// EN
function en_date($datetime) {
    $month  = date('m ', strtotime($datetime));
    $day    = date('j', strtotime($datetime));

    return  en_month($month) . ' ' . $day;
}

function full_en_date($datetime) {
    $month  = date('m ', strtotime($datetime));
    $day    = date('j', strtotime($datetime));
    $year   = date(' Y', strtotime($datetime));

    return  en_month($month) . 
            $day .
            $year;
}

function full_en_datetime($datetime) {
	$month	= date('m ', strtotime($datetime));
    $day    = date('j', strtotime($datetime));
	$year	= date(' Y, ', strtotime($datetime));
	$time	= date('h:i A', strtotime($datetime));

	return	en_month($month) . 
			$day .
			$year . 
			$time;
}

function en_month($month_number) {
	switch ($month_number) {
        case 1:     return "January";
        case 2:     return "February";
        case 3:     return "March";
        case 4:     return "April";
        case 5:     return "May";
        case 6:     return "June";
        case 7:     return "July";
        case 8:     return "August";
        case 9:     return "September";
        case 10:    return "October";
        case 11:    return "November";
        case 12:    return "December";
    }
}


// HE

function he_date($date) {
    $day    = date('j ', strtotime($date));
    $month  = date('m', strtotime($date));

    return  $day . he_month_genetive($month);
}

function full_he_date($date) {
	$day	= date('j ', strtotime($date));
	$month	= date('m', strtotime($date));
	$year	= date(' Y', strtotime($date));

	return	$day .
			he_month_genetive($month) .
			$year;
}

function full_he_datetime($datetime) {
	$day	= date('j ', strtotime($datetime));
	$month	= date('m', strtotime($datetime));
	$year	= date(' Y, ', strtotime($datetime));
	$time	= date('H:i', strtotime($datetime));

	return	$day .
			he_month_genetive($month) .
			$year . 
			$time;
}

function he_month_nominative($month_number) {
    switch ($month_number) {
        case 1:     return "ינואר";
        case 2:     return "פברואר";
        case 3:     return "מרץ";
        case 4:     return "אפריל";
        case 5:     return "מאי";
        case 6:     return "יוני";
        case 7:     return "יולי";
        case 8:     return "אוגוסט";
        case 9:     return "ספטמבר";
        case 10:    return "אוקאובר";
        case 11:    return "נובמבר";
        case 12:    return "דצמבר";
    }
}

function he_month_genetive($month_number) {
    return "ב" . he_month_nominative($month_number);
}


// UK

function uk_date($date) {
    $day    = date('j ', strtotime($date));
    $month  = date('m', strtotime($date));

    return  $day . uk_month_genetive($month);
}

function full_uk_date($date) {
    $day    = date('j ', strtotime($date));
    $month  = date('m', strtotime($date));
    $year   = date(' Y', strtotime($date));

    return  $day .
            uk_month_genetive($month) .
            $year;
}

function full_uk_datetime($datetime) {
    $day    = date('j ', strtotime($datetime));
    $month  = date('m', strtotime($datetime));
    $year   = date(' Y, ', strtotime($datetime));
    $time   = date('H:i', strtotime($datetime));

    return  $day .
            uk_month_genetive($month) . 
            $year . 
            $time;
}

function uk_month_nominative($month_number) {
    switch ($month_number) {
        case 1:     return "січень";
        case 2:     return "лютий";
        case 3:     return "березень";
        case 4:     return "квітень";
        case 5:     return "травень";
        case 6:     return "червень";
        case 7:     return "липень";
        case 8:     return "серпень";
        case 9:     return "вересень";
        case 10:    return "жовтень";
        case 11:    return "листопад";
        case 12:    return "грудень";
    }
}

function uk_month_genetive($month_number) {
    switch ($month_number) {
        case 1:     return "січня";
        case 2:     return "лютого";
        case 3:     return "березня";
        case 4:     return "квітня";
        case 5:     return "травня";
        case 6:     return "червня";
        case 7:     return "липня";
        case 8:     return "серпня";
        case 9:     return "вересня";
        case 10:    return "жовтня";
        case 11:    return "листопада";
        case 12:    return "грудня";
    }
}