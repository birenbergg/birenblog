<?php

function typograf($string){

	$remoteTypograf = new RemoteTypograf('UTF-8');
	$remoteTypograf->htmlEntities();
	$remoteTypograf->br (false);
	$remoteTypograf->p (false);
	$remoteTypograf->nobr (3);
	$remoteTypograf->quotA ('laquo raquo');
	$remoteTypograf->quotB ('bdquo ldquo');

	print $remoteTypograf->processText ($string);
}

function pluralization($n, $forms, $return_number) {

	$f = explode(' ', $forms);
	$amount = $return_number ? $n . ' ' : '';

	$i = 0;

	if ($n % 100 >= 5 && $n % 100 <= 20) {
		return $amount . $f[2];
	} else {
		$i = $n % 10;
		if ($i == 1) {
			return $amount . $f[0];
		} else if ($i >= 2 && $i <= 4) {
			return $amount . $f[1];
		} else {
			return $amount . $f[2];
		}
	}
}