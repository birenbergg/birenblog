<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutdoorsComment extends Model
{
	public $timestamps = false;
	protected $table = 'outdoors_comments';
}