<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZhitieComment extends Model
{
	public $timestamps = false;
	protected $table = 'zhitie_comments';
}