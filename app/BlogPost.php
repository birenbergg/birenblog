<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
	public $timestamps = false;

	public function rubric() {
		return $this->belongsTo('App\BlogRubric');
	}

	public function tags() {
		return $this->belongsToMany('App\BlogTag', 'blog_tag_to_post', 'tag_id', 'post_id')->orderBy('name');
	}

	public function comments() {
		return $this->hasMany('App\BlogComment', 'post_id')->orderBy('date');
	}
}