<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutdoorsPostComponent extends Model
{
	protected $table = 'outdoors_components';

	public function slideshow_images() {
		return $this->hasMany('App\OutdoorsSlideshowImage', 'component_id')->orderBy('index');
	}
}